#include <fstream>
#include <functional>
#include <iostream>

using logFunc_t = std::function<void(const std::string &)>;

int add3(int n) {
    return n+3;
}

int mul2(int n) {
    return n*2;
}

int mycompute(logFunc_t LogFunc, int v0) {
    LogFunc("add3 " + std::to_string(v0));
    const int v1 = add3(v0);
    LogFunc("mul2 " + std::to_string(v1));    
    const int v2 = mul2(v1);
    return v2;
}

int main() {
    std::cout << "this is log-cpp" << std::endl;

    logFunc_t LogCout = [](const std::string & s){ std::cout << s << std::endl;};
    int res1 = mycompute(LogCout, 18);

    std::ofstream ofs("log.txt");
    logFunc_t logFile = [&ofs](const std::string & message){ofs << message << std::endl;};
    int res2 = mycompute(logFile, 18);

    return 0;
}

