import Lib

mynegate :: Maybe Double -> Maybe Double
mynegate (Just x) = Just (negate x)
mynegate Nothing = Nothing 

	

main :: IO ()
main = do
    putStrLn "this is sqrt-hs"
    print $ mynegate $ mysqrt 1764
    print $ mynegate $ mysqrt (-1764)

    
