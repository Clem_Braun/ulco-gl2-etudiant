module Lib where

mysqrt :: Double -> Maybe Double
mysqrt x 
	| x >= 0 = Just $ sqrt x
	| otherwise = Nothing
