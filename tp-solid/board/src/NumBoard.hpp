#pragma once

#include "Board.hpp"

#include <fstream>
#include <iostream>

using namespace std;

class NumBoard : public Board {
    private : 
        unsigned int nbticket;

    public : 

        NumBoard() : nbticket(1) {};

        string getnb(string res){
            string s = to_string(nbticket) + "- " + res;
            nbticket++;
            return s;
        }
};