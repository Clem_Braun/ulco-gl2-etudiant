#include "Board.hpp"
#include "NumBoard.hpp"

void testBoard(Board & b, Reportable & r) {
 std::cout << b.getTitle() << std::endl;
    b.add("item a");
    b.add("item b");
    r.report(b);
}

void testNumBoard(NumBoard & b, Reportable & r){
    std::cout << b.getTitle() << std::endl;
    b.add("item a");
    b.add("item b");
    r.report(b);
}


int main() {

    NumBoard b1;
    ReportStdout r1;
    testNumBoard(b1, r1);
    testBoard(b1, r1);
    return 0;
}

