#pragma once

#include <string>
#include <iostream>


class Reportable {
    public:     
        virtual ~Reportable() = default;   
        virtual void report(const Itemable & itemable) = 0;
};

class ReportStdout : public Reportable{
    public :
        virtual void report(const Itemable & itemable) override {
            for (const std::string & item : itemable.getItems())
                std::cout << item << std::endl;
                std::cout << std::endl;
        }

};

class reportFile : public Reportable{
    private : 
            std::ofstream _ofs;
    public :
        reportFile(const std::string & filename) : _ofs(filename) {}
        virtual void report(const Itemable & itemable) override {
            for (const std::string & item : itemable.getItems())
                _ofs << item << std::endl;
            _ofs<< std::endl;
        }
};