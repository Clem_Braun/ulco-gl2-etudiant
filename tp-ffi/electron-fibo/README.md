
# electron-fibo

- install dependencies:

```sh
npm install
```

- run the app:

```sh
npm start
```

- clean project:

```sh
npm run clean
```

