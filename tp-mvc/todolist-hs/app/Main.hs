import Board
import View
import Text.Read (readMaybe)

loop :: Board -> IO ()
loop b = do
    let i = 0
    putStrLn ""
    printBoard b
    putStrLn ""
    input <- getLine
    let (action:phrase) = words input
    case action of
        "add" -> do
            let ( i, b') = addTodo (unwords phrase) b
            loop b'
        "done" -> do
            putStrLn ("done " ++ (unwords phrase))
            loop b
        "quit" -> do
            putStrLn ("")
        _  -> putStrLn "ERROR"

main :: IO ()
main = loop newBoard


