#include <Task.hpp>
#include <vector>

using namespace std;

class Board{
    public:
    int board_id;
    vector<Task> board_todo;
    vector<Task> board_done;
    Board(){board_id = 1; board_todo = {} ; board_done = {};};
    void addTodo(string str){
        Task t;
        t.id = board_todo.size() +1;
        t.task_name = str;
        board_todo.push_back(t);
    }
    void toDone(int i){
        for(int j=0 ; j < board_todo.size() ; j++){
            if(board_todo[j].id == i){
                board_todo.pop_back();
            }
        }
    }
}


