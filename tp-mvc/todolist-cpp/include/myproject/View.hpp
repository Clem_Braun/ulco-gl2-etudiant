#include <Board.hpp>
#include <Task.hpp>
#include <iostream>
#include <string>

using namespace std; 

void show_Tasks(Task t){
    cout << t.id << "       " << t.task_name << endl;
}

void printBoard (Board b){
    cout << "Todo : " << endl;
    for(int i = 0 ; i < b.board_todo.size(); i++){
        cout << b.board_todo[i].id << "         " << b.board_todo[i].task_name;
    }
    cout << "Done : " << endl;
    for(int i = 0 ; i < b.board_done.size(); i++){
        cout << b.board_done[i].id << "         " << b.board_done[i].task_name;
    }
}